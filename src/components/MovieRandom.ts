import { Movie } from "../data/tmdbUtils";

export function updateMovieRandom(movie: Movie): void {
    const section = document.getElementById('random-movie');
    const title = document.getElementById('random-movie-name');
    const overview = document.getElementById('random-movie-description');
    if (section && movie.backdropPath) {
        section.style.backgroundImage=`url(${movie.backdropPath})`;
        section.style.backgroundSize='cover';
    }
    if (title) {
        title.innerHTML = movie.title;
    }
    if (overview) {
        overview.innerHTML = movie.overview;
    }
}