import { Movie } from "../data/tmdbUtils";
import { FavoriteIcon } from "./FavoriteIcon";
import { MovieListPlacement } from "./MovieList";

export function MovieCard(movie: Movie, placement: MovieListPlacement, isFavorite: boolean = false): HTMLDivElement {
    const card = document.createElement('div');
    const image = document.createElement('img');
    card.id=`${placement}-movie-${movie.id}`;
    card.append(image);
    card.append(FavoriteIcon(isFavorite, placement, movie.id));
    card.append(CardBody(movie.overview, movie.releaseDate));
    card.classList.add('card', 'shadow-sm');    
    if (movie.posterPath) {
        image.src=movie.posterPath;
    }
    return card;
}

function CardBody(overview: string, releaseDate: string): HTMLDivElement {
    const cardBody = document.createElement('div');
    const cardText = document.createElement('p');
    cardBody.append(cardText);
    cardBody.append(ReleaseDate(releaseDate));
    cardText.innerText=overview;
    cardText.classList.add('card-text', 'truncate');
    cardBody.classList.add('card-body');
    return cardBody
}

function ReleaseDate(date: string): HTMLDivElement {
    const releaseDateContainer = document.createElement('div');
    const releaseDate = document.createElement('small');
    releaseDateContainer.append(releaseDate);
    releaseDate.innerText=date;
    releaseDate.classList.add('text-muted');
    releaseDateContainer.classList.add('d-flex', 'justify-content-between', 'align-items-center');
    return releaseDateContainer;
}