import { MovieCard } from './MovieCard';
import { Movie } from '../data/tmdbUtils'

export enum MovieListPlacement {
    main = 'main',
    aside = 'aside'
}

const movieListCSSClass = {
    main: ['col-lg-3', 'col-md-4', 'col-12', 'p-2'],
    aside: ['col-12', 'p-2']
}

export function MovieList(movies: Movie[], favorites: Set<number>, placement: MovieListPlacement): DocumentFragment {
    const list = document.createDocumentFragment();
    movies.forEach(movie => {
        const placeholder = document.createElement('div');
        list.appendChild(placeholder);
        placeholder.appendChild(MovieCard(movie, placement, favorites.has(movie.id)));
        placeholder.classList.add(...movieListCSSClass[placement]);
    })
    return list;
}
