import { MovieListPlacement } from "./MovieList";

const COLOR_FAVORITE = 'red';
const COLOR_REGULAR = '#ff000078';

export function FavoriteIcon(isFavorite: boolean, placement: MovieListPlacement, idSuffix: number): SVGElement {
    const favoriteIconSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    favoriteIconSVG.append(FavoriteIconPath(placement, idSuffix));
    favoriteIconSVG.id=`${placement}-favorite-svg-${idSuffix}`;
    favoriteIconSVG.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
    favoriteIconSVG.setAttribute('stroke', COLOR_FAVORITE);
    favoriteIconSVG.setAttribute('fill', isFavorite ? COLOR_FAVORITE : COLOR_REGULAR);
    favoriteIconSVG.setAttribute('width', '50');
    favoriteIconSVG.setAttribute('height', '50');
    favoriteIconSVG.setAttribute('viewBox', '0 -2 18 22');    
    favoriteIconSVG.classList.add('bi', 'bi-heart-fill', 'position-absolute', 'p-2');
    return favoriteIconSVG;
}

function FavoriteIconPath(placement: MovieListPlacement,idSuffix: number): SVGPathElement {
    const favoriteIconPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    favoriteIconPath.id=`${placement}-favorite-path-${idSuffix}`;
    favoriteIconPath.setAttribute('fill-rule', 'evenodd');
    favoriteIconPath.setAttribute('d', 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z');
    return favoriteIconPath;
}

export function toggleFavoriteColor(favoriteIcon: SVGElement): void {
    const current = favoriteIcon.getAttribute('fill');
    if (current === COLOR_FAVORITE) {
        favoriteIcon.setAttribute('fill', COLOR_REGULAR);
    } else if (current === COLOR_REGULAR) {
        favoriteIcon.setAttribute('fill', COLOR_FAVORITE);
    }
}