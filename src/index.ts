import { fetchPopular } from './data/tmdbData';
import { addSelectCategoryListener } from './handlers/selectCategory';
import { loadFavorite, toggleFavorite, deleteFavorite } from './handlers/manageFavorite';
import { loadMoreMovies, replaceMovieList, setMovieListStatus, RequestData } from './handlers/common';
import { searchMovies } from './handlers/search';
export async function render(): Promise<void> {
    const movieList = document.getElementById('film-container');
    const favoriteList = document.getElementById('favorite-movies');
    const popular = document.getElementById('popular') as HTMLInputElement | null;
    const loadMore = document.getElementById('load-more') as HTMLButtonElement | null;
    const submit = document.getElementById('submit') as HTMLButtonElement | null;
    const search = document.getElementById('search') as HTMLInputElement | null;
    if (popular) {
        popular.checked = true;
        if (movieList) {
            movieList.innerHTML='Loading. Please wait';
            fetchPopular(1).then(response => {
                replaceMovieList(response.results)
                setMovieListStatus(RequestData.popular, response.totalPages)
            });
        }
    }
    loadFavorite();
    if (movieList) {
        movieList.addEventListener('click', toggleFavorite);
    }
    if (favoriteList) {
        favoriteList.addEventListener('click', deleteFavorite);
    }
    if (loadMore) {
        loadMore.addEventListener('click', loadMoreMovies);
    }
    if (submit) {
        submit.addEventListener('click', searchMovies);
    }
    addSelectCategoryListener();
}
