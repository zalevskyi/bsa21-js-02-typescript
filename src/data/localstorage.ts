const KEY = 'favorite'

export class FavoriteMovies {
    static get(): Set<number> {
        const savedData = localStorage.getItem(KEY);
        return savedData ? new Set(JSON.parse(savedData)) : new Set();
    }
    static add(id: number): Set<number> {
        const favorite = this.get();
        favorite.add(id);
        localStorage.setItem(KEY, JSON.stringify(favorite));
        return favorite;
    }
    static delete(id: number): Set<number> {
        const favorite = this.get();
        favorite.delete(id);
        localStorage.setItem(KEY, JSON.stringify(favorite));
        return favorite;
    }
}