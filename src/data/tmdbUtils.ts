const IMAGE_URL = 'https://image.tmdb.org/t/p/original';

export interface Movie {
    id: number;
    title: string;
    overview: string;
    releaseDate: string;
    posterPath: string | null;
    backdropPath: string | null;
}

export interface MovieAPI {
    poster_path: string | null,
    overview: string,
    release_date: string,
    id: number,
    title: string,
    backdrop_path: string | null,
}

export interface MovieJSONList {
    page: number,
    results: Movie[],
    totalPages: number,
    totalResults: number,
}

export interface MovieJSONListAPI {
    page: number,
    results: MovieAPI[],
    total_pages: number,
    total_results: number,
}

export function movieFromMovieAPI(movieAPI: MovieAPI): Movie {
    return {
        id: movieAPI.id,
        title: movieAPI.title,
        posterPath: movieAPI.poster_path ? `${IMAGE_URL}${movieAPI.poster_path}` : null,
        backdropPath: movieAPI.backdrop_path ? `${IMAGE_URL}${movieAPI.backdrop_path}` : null,
        releaseDate: movieAPI.release_date,
        overview: movieAPI.overview,
    }
}

export function mapMovieList(apiResponse: MovieJSONListAPI): MovieJSONList {
    return {
        page: apiResponse.page,
        totalPages: apiResponse.total_pages,
        totalResults: apiResponse.total_results,
        results: apiResponse.results.map(movieFromMovieAPI)
    };
}