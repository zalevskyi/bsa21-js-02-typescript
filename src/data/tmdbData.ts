import { Movie, MovieAPI, MovieJSONList, MovieJSONListAPI, movieFromMovieAPI, mapMovieList } from "./tmdbUtils"

const API_URL = 'https://api.themoviedb.org/3';
const REQUEST_PATH = {
    popular: '/movie/popular',
    top: '/movie/top_rated',
    upcoming: '/movie/upcoming',
    search: '/search/movie',
    movieDetails: '/movie/'
}
const LANG = 'en-US';
const REGION = 'UA';

// Bad practice, but API key is saved in repository for easy solution checking
const API_KEY = '0336f63bf3ba9446a5164339ab4dacf3'

export type FetchList = (page: number) => Promise<MovieJSONList>;
export type FetchSearch = (query: string, page: number) => Promise<MovieJSONList>;

export async function fetchPopular(page: number): Promise<MovieJSONList> {
    const responseJSON = await fetchData(prepareURL(`${API_URL}${REQUEST_PATH.popular}`, page));
    return mapMovieList(responseJSON as unknown as MovieJSONListAPI);
}

export async function fetchUpcoming(page: number): Promise<MovieJSONList> {
    const responseJSON = await fetchData(prepareURL(`${API_URL}${REQUEST_PATH.upcoming}`, page));
    return mapMovieList(responseJSON as unknown as MovieJSONListAPI);
}

export async function fetchTop(page: number): Promise<MovieJSONList> {
    const responseJSON = await fetchData(prepareURL(`${API_URL}${REQUEST_PATH.top}`, page));
    return mapMovieList(responseJSON as unknown as MovieJSONListAPI);
}

export async function fetchSearch(query: string, page: number): Promise<MovieJSONList> {
    const responseJSON = await fetchData(prepareURL(`${API_URL}${REQUEST_PATH.search}`, page, query));
    return mapMovieList(responseJSON as unknown as MovieJSONListAPI);
}

export async function fetchMovieDetails(id: number): Promise<Movie> {
    const responseJSON = await fetchData(prepareURL(`${API_URL}${REQUEST_PATH.movieDetails}${id}`));
    return movieFromMovieAPI(responseJSON as unknown as MovieAPI);
}

export async function fetchData(url: URL): Promise<JSON> {
    const response = await fetch(url.href);
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return await response.json();
}

function prepareURL(endpoint: string, page?: number, query?: string): URL {
    const url = new URL(endpoint);
    url.searchParams.set('api_key', API_KEY);
    url.searchParams.set('language', LANG);
    url.searchParams.set('region', REGION);
    if (page) {
        url.searchParams.set('page', page.toString());
    }
    if (query) {
        url.searchParams.set('query', query);
    }
    return url;
}
