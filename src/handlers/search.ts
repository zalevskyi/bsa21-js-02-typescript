import { fetchSearch } from "../data/tmdbData";
import { replaceMovieList, RequestData, setMovieListStatus } from "./common";

const movieList = document.getElementById('film-container');
const search = document.getElementById('search') as HTMLInputElement | null;
const popular = document.getElementById('popular') as HTMLInputElement | null;
const upcoming = document.getElementById('upcoming') as HTMLInputElement | null;
const topRated = document.getElementById('top_rated') as HTMLInputElement | null;

export function searchMovies(evt: Event) {
    if (movieList) {
        movieList.innerHTML='Loading. Please wait';
        const query = search!.value;
        fetchSearch(query, 1).then(response => {
            replaceMovieList(response.results);
            setMovieListStatus(RequestData.search, response.totalPages, query)
        });        
    }
    if (popular) {
        popular.checked = false;
    }
    if (upcoming) {
        upcoming.checked = false;
    }
    if (topRated) {
        topRated.checked = false;
    }
}