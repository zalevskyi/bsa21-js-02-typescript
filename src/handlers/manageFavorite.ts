import { FavoriteMovies } from "../data/localstorage"
import { toggleFavoriteColor } from "../components/FavoriteIcon"
import { MovieList, MovieListPlacement } from "../components/MovieList";
import { fetchMovieDetails } from "../data/tmdbData";

const favoriteList = document.getElementById('favorite-movies');

export function toggleFavorite({target}: Event) {
    if (target) {
        const idParts = (target as HTMLLIElement).id.split('-')
        if (idParts.length === 4) {
            if (idParts[0] === MovieListPlacement.main && idParts[1] === 'favorite' && idParts[2] === 'path') {
                const movieId = Number(idParts[3]);
                if (FavoriteMovies.get().has(movieId)) {
                    FavoriteMovies.delete(movieId);
                    const favoriteCard = document.getElementById(`${MovieListPlacement.aside}-movie-${movieId}`);
                    if (favoriteCard) {
                        favoriteCard.remove();
                    }
                } else {
                    FavoriteMovies.add(movieId);
                    if (favoriteList) {
                        fetchMovieDetails(movieId).then(movie => {
                            favoriteList.appendChild(MovieList([movie], FavoriteMovies.get(), MovieListPlacement.aside));
                        })
                    }
                }
                const favoriteSVG = document.getElementById(`${MovieListPlacement.main}-favorite-svg-${movieId}`) as unknown as SVGElement | null;
                if (favoriteSVG) {
                    toggleFavoriteColor(favoriteSVG);
                }
            }
        }
    }
}

export function loadFavorite() {
    if (favoriteList) {
        favoriteList.innerHTML='';
        const favorites = FavoriteMovies.get();
        favorites.forEach(id => {
            fetchMovieDetails(id).then(movie => {
                favoriteList.appendChild(MovieList([movie], favorites, MovieListPlacement.aside));
            })
        });
    }
}

export function deleteFavorite({target}: Event) {
    if (target) {
        const idParts = (target as HTMLLIElement).id.split('-')
        if (idParts.length === 4) {
            if (idParts[0] === MovieListPlacement.aside && idParts[1] === 'favorite' && idParts[2] === 'path') {
                const movieId = Number(idParts[3]);
                FavoriteMovies.delete(movieId);
                const favoriteCard = document.getElementById(`${MovieListPlacement.aside}-movie-${movieId}`);
                if (favoriteCard) {
                    favoriteCard.remove();
                }
                const favoriteSVG = document.getElementById(`${MovieListPlacement.main}-favorite-svg-${movieId}`) as unknown as SVGElement | null;
                if (favoriteSVG) {
                    toggleFavoriteColor(favoriteSVG);
                }
            }
        }
    }
}