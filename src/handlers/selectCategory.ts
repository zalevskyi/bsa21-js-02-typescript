import { fetchPopular, fetchUpcoming, fetchTop } from "../data/tmdbData";
import { replaceMovieList, RequestData, setMovieListStatus } from "./common";

const popular = document.getElementById('popular') as HTMLInputElement | null;
const upcoming = document.getElementById('upcoming') as HTMLInputElement | null;
const topRated = document.getElementById('top_rated') as HTMLInputElement | null;
const movieList = document.getElementById('film-container');
const search = document.getElementById('search') as HTMLInputElement | null;

export function addSelectCategoryListener(): void {
    if (popular) {
        popular.addEventListener('change', selectPopular);
    }
    if (upcoming) {
        upcoming.addEventListener('change', selectUpcoming);
    }
    if (topRated) {
        topRated.addEventListener('change', selectTop);
    }
}

function selectTop(evt: Event) {
    if (movieList) {
        movieList.innerHTML='Loading. Please wait';
        if (search) {
            search.value='';
        }
        fetchTop(1).then(res => {
            // check that user did not change category between sending request and getting response
            // element is not null as it was checked
            if (topRated!.checked) {
                replaceMovieList(res.results);
                setMovieListStatus(RequestData.top, res.totalPages)
            }
        });        
    }
}

function selectPopular(evt: Event) {
    if (movieList) {
        movieList.innerHTML='Loading. Please wait';
        if (search) {
            search.value='';
        }
        fetchPopular(1).then(res => {
            // check that user did not change category between sending request and getting response
            // element is not null as it was checked
            if (popular!.checked) {
                replaceMovieList(res.results);
                setMovieListStatus(RequestData.popular, res.totalPages)
            }
        });        
    }
}

function selectUpcoming(evt: Event) {
    if (movieList) {
        movieList.innerHTML='Loading. Please wait';
        if (search) {
            search.value='';
        }
        fetchUpcoming(1).then(res => {
            // check that user did not change category between sending request and getting response
            // element is not null as it was checked
            if (upcoming!.checked) {
                replaceMovieList(res.results);
                setMovieListStatus(RequestData.upcoming, res.totalPages)
            }
        });        
    }
}
