import { MovieList, MovieListPlacement } from "../components/MovieList";
import { updateMovieRandom } from "../components/MovieRandom";
import { FavoriteMovies } from "../data/localstorage";
import { fetchPopular, fetchSearch, fetchTop, fetchUpcoming } from "../data/tmdbData";
import { Movie } from "../data/tmdbUtils";
import { selectRandomItem } from "../utils/utils";


export enum RequestData {
    popular,
    upcoming,
    top,
    search,
}
interface MovieListStatus {
    requestData: RequestData,
    lastPage: number,
    totalPages: number,
    query?: string,
}

const movieList = document.getElementById('film-container');
const loadMore = document.getElementById('load-more') as HTMLButtonElement | null;

const movieListStatus: MovieListStatus = {
    requestData: RequestData.popular,
    lastPage: 1,
    totalPages: 1,
    query: undefined,
}

export function replaceMovieList(movies: Movie[]): void {
    if (movieList) {
        movieList.innerHTML='';
        updateMovieRandom(selectRandomItem(movies));
        movieList.appendChild(MovieList(movies, FavoriteMovies.get(), MovieListPlacement.main));
    }
}

export function setMovieListStatus(requestData: RequestData, totalPages: number, query?: string): void {
    movieListStatus.requestData = requestData;
    movieListStatus.lastPage = 1;
    movieListStatus.totalPages = totalPages;
    movieListStatus.query = query;
    if (movieListStatus.lastPage === movieListStatus.totalPages) {
        loadMore!.style.setProperty('display', 'none')
    } else {
        loadMore!.style.setProperty('display', 'unset')
    }
}

export function loadMoreMovies(evt: Event) {
    movieListStatus.lastPage += 1;
    if (movieListStatus.requestData === RequestData.popular) {
        fetchPopular(movieListStatus.lastPage).then(response => {
            movieList!.appendChild(MovieList(response.results, FavoriteMovies.get(), MovieListPlacement.main));
        })
    }
    if (movieListStatus.requestData === RequestData.upcoming) {
        fetchUpcoming(movieListStatus.lastPage).then(response => {
            movieList!.appendChild(MovieList(response.results, FavoriteMovies.get(), MovieListPlacement.main));
        })
    }
    if (movieListStatus.requestData === RequestData.top) {
        fetchTop(movieListStatus.lastPage).then(response => {
            movieList!.appendChild(MovieList(response.results, FavoriteMovies.get(), MovieListPlacement.main));
        })
    }
    if (movieListStatus.requestData === RequestData.search) {
        if (movieListStatus.query) {
            fetchSearch(movieListStatus.query, movieListStatus.lastPage).then(response => {
                movieList!.appendChild(MovieList(response.results, FavoriteMovies.get(), MovieListPlacement.main));
            })
        }
    }
    if (movieListStatus.lastPage === movieListStatus.totalPages) {
        loadMore!.style.setProperty('display', 'none')
    }
}